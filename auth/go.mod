module gitlab.com/hookactions/go-utils/auth

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
	gitlab.com/hookactions/go-utils/crypto v0.0.0-20190823145105-72b2e5f698af
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
)
