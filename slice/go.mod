module gitlab.com/hookactions/go-utils/slice

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.3.0
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
)
